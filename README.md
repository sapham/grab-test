#### Grab Test

##### Task

Use any language to setup a Wordpress & monitoring server. We are looking for a functional script.

##### Descriptions

In these scripts, I will install wordpress and monitoring tool to monitor linux server. 

Monitor include: InfluxDB, Telegraf and Grafana

##### Install

- Just run `install.sh`


##### Monitor


After everything are installed, Go to grafana UI and import dashboard using template json file in dashboards directory.

