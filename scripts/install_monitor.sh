#!/bin/bash

influxdb_db_name=monitor
influxdb_db_username=monitor

echo "Install monitor tool, include influxdb, telegraf and grafana"
sleep 2

echo "Check influxdb install"
check_influxdb_installed=`dpkg -l | grep -c influxdb`
if [ $check_influxdb_installed -lt 1 ]; then
 
    echo "Installing influxdb"
    curl https://dl.influxdata.com/influxdb/nightlies/influxdb_nightly_amd64.deb -o /tmp/influxdb_nightly_amd64.deb
    if [ -f /tmp/influxdb_nightly_amd64.deb ]; then
    	dpkg -i /tmp/influxdb_nightly_amd64.deb
    
    	systemctl restart influxdb
    	while true;
    	do 
    		check_influxdb_started=`netstat -lntp | grep -c 8086`
    		if [ $check_influxdb_started -eq 1 ]; then
    			echo "Done"
    			break;
    		fi
    	done
    	echo "Create database and user for influxdb"
            curl http://localhost:8086/query --data-urlencode "q=CREATE USER admin WITH PASSWORD '$influxdb_admin_password' WITH ALL PRIVILEGES" >> /dev/null
    	echo "Created admin password"
    
    	curl http://localhost:8086/query --data-urlencode "q=CREATE USER $influxdb_db_username WITH PASSWORD '$influxdb_db_password'" >> /dev/null
    	echo "Created db user $influxdb_db_username"
    	
            curl http://localhost:8086/query --data-urlencode "q=CREATE DATABASE $influxdb_db_name" >> /dev/null
    	echo "Created Db $influxdb_db_name"
            curl http://localhost:8086/query --data-urlencode "q=GRANT ALL PRIVILEGES ON $influxdb_db_name.* to $influxdb_db_username" >> /dev/null
            
    else
    	echo "Cannot download influxdb, please check your connection"
    fi

else
	echo "Influxdb already installed"
fi

check_telegraf_installed=`dpkg -l | grep -c telegraf`
if [ $check_telegraf_installed -lt 1 ]; then
    echo "Installing telegraf"
    curl https://dl.influxdata.com/telegraf/releases/telegraf_1.7.4-1_amd64.deb -o /tmp/telegraf_1.7.4-1_amd64.deb
    
    if [ -f /tmp/telegraf_1.7.4-1_amd64.deb ]; then
    	dpkg -i /tmp/telegraf_1.7.4-1_amd64.deb
    
    	echo "Configure telegraf"
    
cat << EOF > /etc/telegraf/telegraf.conf

[global_tags]
[agent]
  interval = "10s"
  round_interval = true
  metric_batch_size = 1000
  metric_buffer_limit = 10000
  collection_jitter = "0s"
  flush_interval = "10s"
  flush_jitter = "0s"
  precision = ""
  debug = false
  quiet = false
  logfile = ""
  hostname = ""
  omit_hostname = false

[[outputs.influxdb]]
  urls = ["http://127.0.0.1:8086"]
  database = "$influxdb_db_name"
  username = "$influxdb_db_username"
  password = "$influxdb_db_password"

[[inputs.cpu]]
  percpu = true
  totalcpu = true
  collect_cpu_time = false
  report_active = false

[[inputs.disk]]
  ignore_fs = ["tmpfs", "devtmpfs", "devfs"]

[[inputs.diskio]]
[[inputs.kernel]]
[[inputs.mem]]
[[inputs.processes]]
[[inputs.swap]]
[[inputs.system]]
[[inputs.net]]

EOF
    	systemctl restart telegraf
    
    else
    	echo "Cannot download telegraf, please check your connection"
    fi
else
	echo "Telegraf already installed"
fi

check_grafana_installed=`dpkg -l | grep -c grafana`
if [ $check_grafana_installed -lt 1 ]; then
    echo "Installing Grafana"
    
    curl https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_5.2.4_amd64.deb  -o /tmp/grafana_5.2.4_amd64.deb
    
    if [ -f /tmp/grafana_5.2.4_amd64.deb ]; then
    	dpkg -i /tmp/grafana_5.2.4_amd64.deb
    	systemctl restart grafana-server
    	
    	echo "Configure grafana"
	echo "Add datasource"
		curl http://admin:admin@localhost:3000/api/datasources -H "Content-Type: application/json" -d '{"name": "influxdb", "type": "influxdb", "url": "http://'$ip_address':8086", "database": "'$influxdb_db_name'", "user": "'$influxdb_db_username'", "password": "'$influxdb_db_password'", "access": "direct" }' -X POST
		
	
    
    else
    	echo "Cannot download grafana, please check your connection"
    fi

else
	echo "Grafana already installed"
fi
