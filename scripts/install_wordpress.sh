#!/bin/bash

wordpress_db_name=wordpress
wordpress_db_username=wpusername

echo "Installing Apache2 web server"
sleep 3
sudo apt-get install apache2 libapache2-mod-php7.0 -y  >> /dev/null


echo "Installing MySQL Database server"
sleep 3
echo  mysql-server mysql-server/root_password password $mysql_root_password | debconf-set-selections 
echo  mysql-server mysql-server/root_password_again password $mysql_root_password | \
	debconf-set-selections 

sudo apt install mariadb-server -y  >> /dev/null

check_exist=`mysql -uroot -p$mysql_root_password --skip-column-names -e "show databases like '$wordpress_db_name'"`

if [ "$check_exist" == "$wordpress_db_name" ]; then
echo "Database exist, drop now"
mysql -uroot -p$mysql_root_password -e "DROP DATABASE $wordpress_db_name"
fi

cat << EOF | mysql -uroot -p$mysql_root_password
CREATE DATABASE $wordpress_db_name default character set utf8;
GRANT ALL PRIVILEGES ON $wordpress_db_name.* TO '$wordpress_db_username'@'localhost' IDENTIFIED BY '$wordpress_db_password';
GRANT ALL PRIVILEGES ON $wordpress_db_name.* TO '$wordpress_db_username'@'%' IDENTIFIED BY '$wordpress_db_password';
FLUSH PRIVILEGES;
EOF


echo "Install PHP"
sleep 3
sudo apt install -y php php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc php7.0-mysql >> /dev/null

echo "Configure apache2"
sleep 3
cat << EOF > /etc/apache2/mods-enabled/dir.conf
<IfModule mod_dir.c>
        DirectoryIndex index.php index.html index.cgi index.pl index.php index.xhtml index.htm
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

systemctl restart apache2

echo "Install apache2"
sudo apt install curl -y >> /dev/null
curl https://wordpress.org/latest.tar.gz -o /tmp/latest.tar.gz
tar -xvf /tmp/latest.tar.gz -C /tmp/ >> /dev/null
rm -rf /tmp/latest.tar.gz 
cp -r /tmp/wordpress/* /var/www/html
cp /var/www/html/wp-config-sample.php  /var/www/html/wp-config.php 
chown -R www-data:www-data /var/www/html/


echo "Configure wordpress"
sed -i "s/database_name_here/$wordpress_db_name/g" /var/www/html/wp-config.php
sed -i "s/username_here/$wordpress_db_username/g" /var/www/html/wp-config.php
sed -i "s/password_here/$wordpress_db_password/g" /var/www/html/wp-config.php
