#!/bin/bash
export LC_ALL=C
echo "Script to install wordpress and monitor server on Ubuntu 16.04"
sleep 2
echo "Please enter MySQL Root password"
read -s mysql_root_password
sleep 1
echo "=============DONE==============="
echo "Please enter Wordpress Database password"
read -s wordpress_db_password
sleep 1
echo "=============DONE==============="

echo "Enter your influxdb admin password"
read -s influxdb_admin_password
echo "=============DONE==============="

echo "Enter your influxdb database password"
read -s influxdb_db_password
echo "=============DONE==============="


ip_address=`hostname -i | cut -d " " -f 2`

echo "Detect your operating system..."
sleep 1
if [ -f /etc/lsb-release ]; 
then
    echo "You are running Debian Distro"
    echo "Starting install wordpress"
    sleep 3
    source ./scripts/install_wordpress.sh
    echo "Starting install monitor"
    sleep 3
    source ./scripts/install_monitor.sh
    echo "================================================================="
    echo "Installed WORDPRESS AND MONITOR"
    echo "Access wordpress and setup http://"$ip_address
    echo "Access grafana UI http://"$ip_address":3000"
    echo "Grafana credential: admin/admin"
    echo "Access Grafana and import dashboard file in dashboards directory"
    echo "================================================================="
elif [ -f /etc/centos-release ];
then
    echo "You are running CentOS Distro"]
    echo "We have not support your Distro yet"
    exit 1
else
    echo "Cannot detect your operation system"
    exit 1
fi

